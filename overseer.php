<?php

include __DIR__.'/vendor/autoload.php';

use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
use Google\Spreadsheet\SpreadsheetService;
use Discord\DiscordCommandClient;

const TOKEN           = 'ya29.GltNBMAwUTjuHMu1VGfK0a0e4pqwx_mM07a066ik9pzvzHsZWBYeOBCYCILyCm4KzEr3RkAwCma_e2ft1UuqRYA22n3VsG2uVmTbDpyVZWRejm08cbS58kSFn6ix';
const DISCORD_TOKEN   = 'MzExNzAwMzU2NDUzMjM2NzM3.XawC3w.gL9EYKWtsEbD3F_RGAciwCOd9xE';
const SHEET_ID        = '1DybbJNN2hUD8VkZYlPWW-K1n_409w_OJ4R23nQmgWnQ';
const WORKSHEET_NAME  = 'MVP SPAWN';
const SERVER_ID       = '282875011633512448';
const CHANNEL_ID      = '629295714261794826';
const ROLE            = '<@&635373164968542208>';

$sheet_data  = [];
$to_send     = [];
$worksheet   = [];
$discord = new DiscordCommandClient([
  'token' => DISCORD_TOKEN,
  'discordOptions' => [
    'disabledEvents' => [
      'PRESENCE_UPDATE'
    ]
  ]
]);

$discord->on('ready', function ($discord)
{
  overseer_init($discord);
  echo 'Overseer init success.';
});

function google_start()
{
  $serviceRequest = new DefaultServiceRequest(TOKEN);
  ServiceRequestFactory::setInstance($serviceRequest);

  $spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
  $worksheetfeed = $spreadsheetService->getPublicSpreadsheet(SHEET_ID);
  $worksheet = $worksheetfeed->getByTitle(WORKSHEET_NAME);
  $GLOBALS['worksheet'] = $worksheet;
  update_data();
}

function update_data()
{
  $worksheet = $GLOBALS['worksheet'];
  $list_feed = $worksheet->getListFeed();

  $result = [];
  foreach ($list_feed->getEntries() as $entries) {
    $item = $entries->getValues();
    array_push($result, $item);
  }

  $GLOBALS['sheet_data'] = $result;
}

function overseer_init($discord)
{
  ini_set('memory_limit', '200M');
  date_default_timezone_set('Asia/Hong_Kong');
  set_time_limit(300);

  $discord->registerCommand('status', function ($message)
  {
    update_data();
    $content = parse_content($message->content, 'status');
    $result = check_mvp_status($content);

    if (   !$result
        || strlen($content) == 0)
    {
      $result = 'MVP name not found. To check MVP status enter *status <MVP_NAME_OR_TAG>.* i.e. *status Moonlight Flower*';
    }

    $message->channel->sendMessage($result);
  });

  $discord->registerCommand('closest', function ($message)
  {
    update_data();
    $result = get_closest_mvp();

    if (!$result)
    {
      $result = 'We have no record that we have killed an MVP.';
    }

    $message->channel->sendMessage($result);
  });

  $discord->loop->addPeriodicTimer(5, function ($discord)
  {
    $to_send = $GLOBALS['to_send'];

    if (count($GLOBALS['to_send']) > 0)
    {
      send_message($GLOBALS['to_send'][0]);
      array_shift($GLOBALS['to_send']);
    }
  });

  $discord->loop->addPeriodicTimer(60, function($discord)
  {
    update_data();
    $data = $GLOBALS['sheet_data'];
    $result = array();

    for ($i = 0; $i < count($data); $i++)
    {
      $time      = $data[$i]['time'];
      $to_time   = strtotime($time);
      $from_time = time();
      $diff      = ceil(round(($to_time - $from_time) / 60,2));

      if (   $diff <= 10
          && strtolower($time) != '12:00 am')
      {
        $data[$i]['diff'] = $diff;
        array_push($result, $data[$i]);
      }
    }

    for ($i = 0; $i < count($result); $i++)
    {
      $return  = create_mvp_message($result[$i]);
      $message = $return['message'];
      $send    = $return['send'];

      if ($send)
      {
        array_push($GLOBALS['to_send'], $message);
      }
    }
  });
}

function create_mvp_message($item, $get_all=false)
{
  $send    = false;
  $message = '';

  if (   $item['diff'] == 10
      || $item['diff'] == 5)
  {
    $send = true;
    $message = '`'.$item['boss'].'`{{LOC}} early spawn will start in `'.$item['diff'].' mins.`';

    if ($item['loc'] != '')
    {
      $message = str_replace('{{LOC}}', ' in `'.$item['loc'].'`', $message);
    }
    else
    {
      $message = str_replace('{{LOC}}', '', $message);
    }
  }
  else if ($item['diff'] == 2)
  {
    $send = true;
    $message = '`'.$item['boss'].'`{{LOC}} early spawn will start in `'.$item['diff'].' mins.` Please get ready!';

    if ($item['loc'] != '')
    {
      $message = str_replace('{{LOC}}', ' in `'.$item['loc'].'`', $message);
    }
    else
    {
      $message = str_replace('{{LOC}}', '', $message);
    }
  }
  else if ($item['diff'] == 0)
  {
    $send = true;
    $message = '`'.$item['boss'].'`{{LOC}} early spawn has started! Please check it out..';

    if ($item['loc'] != '')
    {
      $message = str_replace('{{LOC}}', ' in `'.$item['loc'].'`', $message);
    }
    else
    {
      $message = str_replace('{{LOC}}', '', $message);
    }
  }
  else if (   $item['diff'] == -3
           || $item['diff'] == -5
           || $item['diff'] == -10)
  {
    $send = true;
    $message = 'It\'s been `'.abs($item['diff']).' mins.` since `'.$item['boss'].'\'s`{{LOC}} early spawn.. Please check it out.';

    if ($item['loc'] != '')
    {
      $message = str_replace('{{LOC}}', ' in `'.$item['loc'].'`', $message);
    }
    else
    {
      $message = str_replace('{{LOC}}', '', $message);
    }
  }
  else if ($item['diff'] == -20)
  {
    $send = true;
    $message = 'It\'s been `'.abs($item['diff']).' mins.` since `'.$item['boss'].'\'s`{{LOC}} early spawn.. Did we miss it?';

    if ($item['loc'] != '')
    {
      $message = str_replace('{{LOC}}', ' in `'.$item['loc'].'`', $message);
    }
    else
    {
      $message = str_replace('{{LOC}}', '', $message);
    }
  }

  return array(
    'send' => $send,
    'message' => $message
  );
}

function check_mvp_status($name)
{
  $result = find_mvp($name);

  if ($result)
  {
    $time  = $result['time'];
    $boss  = $result['boss'];

    if (   strtolower($time) == "12:00 am"
        || !isset($time))
    {
      $result = 'We have no record that `'.$boss.'` was killed...';
    }
    else
    {
      $message   = '`{MVP}` early spawn will start in approximately `{TIME} mins.` from now.';
      $diff      = get_diff(time(), strtotime($time));

      if ($diff < 0)
      {
        $result = '`'.$boss.'` has spawned `'.abs($diff).' mins.` ago! Please check it out!';
      }
      else
      {
        $result = str_replace(
          array(
            '{MVP}',
            '{TIME}'
          ), array(
            $boss,
            $diff
          ), $message);
      }
    }
  }

  return $result;
}

function get_diff($from_time, $to_time)
{
  return floor(round(($to_time - $from_time) / 60,2));
}

function get_closest_mvp()
{
  $data         = $GLOBALS['sheet_data'];
  $return_value = false;
  $closest      = $data[0];

  for ($i = 0; $i < count($data); $i++)
  {
    if (   strtolower($closest['time']) == '12:00 am'
        && strtolower($data[$i]['time']) != '12:00 am')
    {
      $return_value = true;
      $closest = $data[$i];
      continue;
    }
    else if (strtolower($data[$i]['time']) == '12:00 am')
    {
      continue;
    }

    $diff_closest = get_diff(time(), strtotime($closest['time']));
    $diff_item    = get_diff(time(), strtotime($data[$i]['time']));

    if ($diff_item < $diff_closest)
    {
      $closest = $data[$i];
      $return_value = true;
    }
  }

  if ($return_value)
  {
    $mvp     = $closest['boss'];
    $loc     = $closest['loc'];
    $diff    = get_diff(time(), strtotime($closest['time']));

    if ($diff < 0)
    {
      $message = '`'.$mvp.'`{{LOC}} has spawned `'.abs($diff).' mins.` ago. Please check it out!';
    }
    else
    {
      $message = 'Closest to spawn is `'.$mvp.'`{{LOC}} in about `'.$diff.' mins.`';
    }

    if ($loc != '')
    {
      $message = str_replace('{{LOC}}', ' in `'.$loc.'`', $message);
    }
    else
    {
      $message = str_replace('{{LOC}}', '', $message);
    }

    $return_value = $message;
  }

  return $return_value;
}

function find_mvp($search)
{
  $data = $GLOBALS['sheet_data'];
  $return_value = false;

  for ($i = 0; $i < count($data); $i++)
  {
    $name   = strtolower($data[$i]['boss']);
    $search = strtolower($search);
    $tags   = isset($data[$i]['tags'])
      ? explode(',', strtolower($data[$i]['tags'])) : [];

    if (   $search == $name
        || in_array($search, $tags))
    {
      $return_value = $data[$i];
      break;
    }
  }

  return $return_value;
}

function parse_content($message, $command)
{
  $position = strrpos($message, $command);
  $cmd_len  = strlen($command);
  return trim(substr_replace($message, '', 0, $position + $cmd_len));
}

function send_message($message)
{
  global $discord;
  $guild = $discord->guilds->get('id', SERVER_ID);
  $mvp   = $guild->channels->get('id', CHANNEL_ID);
  $mvp->sendMessage(ROLE.' '.$message);
}

google_start();
$discord->run();
